# Python Minecraft Launcher

![bilibili:UID603295105;](https://img.shields.io/badge/BiliBili-UID603295105-00aeec?logo=bilibili&style=flat-square)![Github: e2662020;](https://img.shields.io/badge/GitHub-e2662020-25292e?logo=github&style=flat-square) ![gitee: e2662020;](https://img.shields.io/badge/Gitee-e2662020-fe7300?logo=gitee&style=flat-square)

## 警告

本程序并未公布正式版，如果您是体验者，请在克隆后将约等于48行的debug = True改为debug = False

这样可以关闭开发者模式

## 视频介绍

[b站](https://www.bilibili.com/video/BV1Yt4y1c7WF)



## 使用方法

### 依赖

| 库名                   | 安装方法                            |
| :--------------------- | ----------------------------------- |
| python                 | 见下                                |
| logging                |                                     |
| sys                    |                                     |
| os                     |                                     |
| PySide6                | pip3 install PySide6                |
| docopt                 | pip3 install docopt                 |
| minecraft_launcher_lib | pip3 install minecraft-launcher-lib |
| subprocess             |                                     |
| webbrowser             |                                     |
| typing                 | pip3 install typing                 |
| IPython                | pip3 install ipython                |
| time                   |                                     |
| requests               | pip3 install requests               |

____________________

### 安装Python

如果您使用ubuntu系统

```
sudu apt-get python3
```

如果您使用centos系统

```
sudo yum -y install python3
```

如果您使用的是linux桌面发行版

[Download Python | Python.org](https://www.python.org/downloads/)

如果您使用macos系统

brew安装

```
brew install python3
```

pkg安装

[Download Python | Python.org](https://www.python.org/downloads/)

如果您使用的是Windows系统

[Download Python | Python.org](https://www.python.org/downloads/)

### 命令

您可以通过 python3 mine.py help查看更多

### 开发文档

[ReadtheDocs](https://python-minecraft-luncher.readthedocs.io/zh_CN/latest/index.html)

[GitBook](https://e2662020s-organization.gitbook.io/pythonminecraftlauncher/)

[GitHubWiki](https://github.com/e2662020/Python-Minecraft-Luncher/wiki)

[Word](/doc/开发教程.docx)

[Office](https://1drv.ms/w/s!AkQCjlWvIVpVgxgEBRKS4A3K3EtL?e=BaHgVa)
